﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ISC.WSIntegracionRef;
using System.Data;

namespace ISC
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            WSIntegracionSoapClient client = new WSIntegracionSoapClient();
            DataSet ds = client.HelloWorld();
            string texto = "";
            foreach (DataRow dsr in ds.Tables[0].Rows)
            {
                texto += dsr["DESCRIPCION"].ToString() + " ";
            }
            TextBox1.Text = texto;
        }
    }
}