﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Data;

/// <summary>
/// Descripción breve de WSIntegracion
/// WS ISC CON CONEXION A LA BD ORACLE
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
// [System.Web.Script.Services.ScriptService]
public class WSIntegracion : System.Web.Services.WebService
{

    public WSIntegracion()
    {

        //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
        //InitializeComponent(); 
    }

    [WebMethod]
    public DataSet SqlSelect()
    {
        OracleConnection cnn = openConnectOracle();
        try
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnn;
            cmd.CommandText = "SELECT * FROM CAPACITACION";
            cmd.CommandType = CommandType.Text;

            OracleDataAdapter da = new OracleDataAdapter(cmd);
            OracleCommandBuilder cb = new OracleCommandBuilder(da);
            DataSet ds = new DataSet();
            da.Fill(ds);

            closeConnectionOracle(cnn);
            return ds;
        }
        catch (Exception)
        {
            closeConnectionOracle(cnn);
            return null;
        }
    }

    [WebMethod]
    public bool StoredProcedure(string value)
    {
        OracleConnection cnn = openConnectOracle();
        try
        {
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = cnn;
            cmd.CommandText = "pkg_capacitacion.cc";
            cmd.CommandType = CommandType.StoredProcedure;

            OracleParameter p1 = cmd.Parameters.Add("texto", OracleDbType.Varchar2, ParameterDirection.Input);
            p1.Value = value;

            cmd.ExecuteNonQuery();

            closeConnectionOracle(cnn);
            return true;
        }
        catch (Exception)
        {
            closeConnectionOracle(cnn);
            return false;
        }
    }

    private OracleConnection openConnectOracle()
    {
        OracleConnection cnn = new OracleConnection();
        string cnnString = "Data Source=localhost:1521/xe;User Id=admin_isc;Password=uu4ll5sp";
        cnn.ConnectionString = cnnString;
        try
        {
            cnn.Open();
            return cnn;
        }
        catch (Exception)
        {
            return null;
        }
    }

    private bool closeConnectionOracle(OracleConnection cnn)
    {
        try
        {
            cnn.Close();
            cnn.Dispose();
            return true;
        }
        catch (Exception)
        {
            return false;
        }
        
    }

}
